package models;

import interfaces.Movable;

public class MovablePoint implements Movable {
     private int x;
     private int y;
     private int xSpeed;
     private int ySpeed;


     public MovablePoint(int x, int y, int xSpeed, int ySpeed) {
          this.x = x;
          this.y = y;
          this.xSpeed = xSpeed;
          this.ySpeed = ySpeed;
     }


     @Override
     public String toString() {
          return "MovablePoint " + "(" + x + "," + y + ")" + "," + "Speed =" + "(" + xSpeed +"," + ySpeed + ")";
     }

     
     @Override
     public void moveUp() {
          ySpeed++;
     }
     

     @Override
     public void moveDown() {
          ySpeed--;
     }

     @Override
     public void moveLeft() {
          xSpeed--;
     }

     @Override
     public void moveRight() {
          xSpeed++;
     }

}
