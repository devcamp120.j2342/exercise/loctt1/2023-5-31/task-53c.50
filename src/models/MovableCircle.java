package models;

import interfaces.Movable;

public class MovableCircle implements Movable{
     private int radius;
     private MovablePoint center;


     public MovableCircle(int radius, MovablePoint center) {
          this.radius = radius;
          this.center = center;
     }


     @Override
     public String toString() {
          return this.center + ", radius = " + this.radius; 
     }

     @Override
     public void moveUp() {
          this.center.moveUp();
     }

     @Override
     public void moveDown() {
          this.center.moveDown();
     }

     @Override
     public void moveLeft() {
          this.center.moveLeft();
     }

     @Override
     public void moveRight() {
          this.center.moveRight();
     }

     
     
}
