import models.MovableCircle;
import models.MovablePoint;

public class App {
    public static void main(String[] args) throws Exception {
        MovablePoint point1 = new MovablePoint(2, 4, 1, 2);
        System.out.println("Point 1:");
        point1.moveUp();
        point1.moveUp();
        point1.moveRight();
        point1.moveRight();
        System.out.println(point1.toString());
        System.out.println("-----------------------------------");

       MovableCircle circle1 = new MovableCircle(2, point1);
        System.out.println("Circle:");
        circle1.moveUp();
        circle1.moveLeft();
        circle1.moveDown();
        circle1.moveRight();
        System.out.println(circle1.toString());
        System.out.println("-----------------------------------");


    }
}
